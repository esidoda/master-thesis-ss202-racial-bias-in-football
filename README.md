# Master Thesis
## Racial bias in the communication about football players on different social media platforms
The goal of this project is to investigate whether players with darker skin tone are commented on differently than players with lighter skin tone by analyzing football-related subreddits and Twitter.


Most current approaches to detecting racial bias in sports focus on studying this social phenomenon in different forms. In recent years, the use of online social media has created a new environment for football fans to interact with each other, comment on the game, and discuss various sporting events. This has been accompanied by an increase in hatred and online discrimination against footballers. This study develops
and employs a methodology to collect Reddit comments and tweets on footballers during a four-season time and investigate the presence of racial bias. This thesis reports results from a dataset of more than 13,000 Reddit comments and 55,000 tweets.

The results of this study show that there is no influence of the skin color of the football players on the sentiment of the comments, but there are other factors that play an important factor in this relationship, like the performance, age, and popularity of football players. Further, it was found that skin color affects the category of the comments players are receiving on online social media, indicating subtle forms of racial
discrimination. The large-scale football commentary on social media corpus annotated with race is publicly released, to motivate further research into characterizing
racial bias in football.
